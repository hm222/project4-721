use serde_json::{json, Value};
use serde::{Serialize, Deserialize};
use lambda_runtime::{handler_fn, Context, Error};

#[derive(Debug, Deserialize, Serialize)]
struct DynamoRequest {
    table_name: String,
    employer: String,
    email: String,
}

async fn extract_handler(event: Value, _: Context) -> Result<Value, Error> {
    let request: DynamoRequest = serde_json::from_value(event).map_err(|e| {
        eprintln!("Error parsing event: {:?}", e);
        Error::from(e)
    })?;
    let resp = json!(request);
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_runtime::run(handler_fn(extract_handler)).await?;
    Ok(())
}