use aws_sdk_dynamodb::{Client, Region};
use aws_config::meta::region::RegionProviderChain;
use std::collections::HashMap;
use aws_sdk_dynamodb::model::AttributeValue;
use lambda_runtime::{Context, Error, handler_fn};
use serde::{Serialize, Deserialize};
use serde_json::{Value, json};

#[derive(Debug, Deserialize, Serialize)]
struct TransformedRequest {
    table_name: String,
    item: HashMap<String, String>,
}

fn convert_to_attribute_values(items: HashMap<String, String>) -> HashMap<String, AttributeValue> {
    items
        .into_iter()
        .map(|(key, value)| (key, AttributeValue::S(value)))
        .collect()
}

async fn load_handler(event: Value, _: Context) -> Result<Value, Error> {
    let region_provider = RegionProviderChain::default_provider().or_else(Region::new("us-east-2"));
    let config = aws_config::from_env().region(region_provider).load().await;
    let client = Client::new(&config);

    let transformed_request: TransformedRequest =  serde_json::from_value(event).map_err(|e| {
        eprintln!("Error parsing event: {:?}", e);
        Error::from(e)
    })?;
    println!("{:?}", transformed_request);
    match client.put_item()
        .table_name(transformed_request.table_name)
        .set_item(Some(convert_to_attribute_values(transformed_request.item)))
        .send().await {
        Ok(_) => Ok(json!({"message": "Item inserted/updated successfully"})),
        Err(e) => {
            eprintln!("DynamoDB error: {:?}", e);
            Err(Error::from(e))
        },
    }
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_runtime::run(handler_fn(load_handler)).await?;
    Ok(())
}
