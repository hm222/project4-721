# ETL Pipeline for DynamoDB

## Project Overview

This project implements an Extract, Transform, Load (ETL) pipeline designed to process data and store it in a DynamoDB table. It utilizes AWS Step Functions to orchestrate the workflow, which involves extracting data from incoming requests, transforming the data into the required format, and loading it into a DynamoDB table with "Employer" and "Email" columns. 

## Architecture

The ETL pipeline is composed of three main components:

- **Extractor Lambda Function**: Extracts data from incoming requests.
- **Transformer Lambda Function**: Transforms data into a structure recognized by DynamoDB, specifically targeting the "Employer" and "Email" schema.
- **Loader Lambda Function**: Loads the transformed data into DynamoDB.

These components are orchestrated using AWS Step Functions, ensuring that the data flows seamlessly from extraction through to loading.

## Prerequisites

- AWS Account
- AWS CLI configured
- Knowledge of AWS Lambda, DynamoDB, and Step Functions

## Deployment

### Step Functions

1. Create each Lambda function (`Extractor`, `Transformer`, `Loader`) in the AWS Lambda console or via the AWS CLI.
2. Grant the necessary permissions to each Lambda function for them to perform their operations (e.g., DynamoDB access for the Loader function).
3. Define your state machine in AWS Step Functions, linking the Lambda functions in the sequence: Extractor -> Transformer -> Loader.
4. Deploy the state machine and note the ARN for execution.

### DynamoDB Table Setup

Ensure you have a DynamoDB table created with the following schema:

- **PrimaryKey**: "Employer".
- **Columns**: Ensure "Employer" and "Email" columns are defined as per your data structure.

## Execution

Trigger the ETL process by starting an execution of your Step Functions state machine, providing the necessary input data. This can be done through the AWS Management Console.

## Monitoring

- **AWS Lambda**: Monitor the execution logs in CloudWatch to ensure each function performs as expected.
- **AWS Step Functions**: Check the execution status and history in the AWS Management Console to monitor the workflow.
- **DynamoDB**: Validate the data insertion by querying your DynamoDB table.