use serde::{Serialize, Deserialize};
use serde_json::{json, Value};
use std::collections::HashMap;
use lambda_runtime::{handler_fn, Context, Error};

#[derive(Deserialize)]
struct DynamoRequest {
    table_name: String,
    employer: String,
    email: String,
}

#[derive(Debug, Deserialize, Serialize)]
struct TransformedRequest {
    table_name: String,
    item: HashMap<String, String>,
}

async fn transform_handler(event: Value, _: Context) -> Result<Value, Error> {
    let request: DynamoRequest = serde_json::from_value(event).map_err(|e| {
        eprintln!("Error parsing event: {:?}", e);
        Error::from(e)
    })?;
    let mut item = HashMap::new();
    item.insert("Employer".to_string(), request.employer.clone());
    item.insert("Email".to_string(), request.email.clone());
    
    let resp = TransformedRequest {
        table_name: request.table_name,
        item,
    };

    let json_resp = json!(resp);
    Ok(json_resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_runtime::run(handler_fn(transform_handler)).await?;
    Ok(())
}
